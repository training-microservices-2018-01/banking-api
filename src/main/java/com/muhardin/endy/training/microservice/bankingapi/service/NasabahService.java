package com.muhardin.endy.training.microservice.bankingapi.service;

import com.muhardin.endy.training.microservice.bankingapi.dto.Nasabah;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "nasabahservice", fallback = NasabahServiceFallback.class)
public interface NasabahService {

    @RequestMapping(method = RequestMethod.GET, value = "/nasabah/list")
    public Iterable<Nasabah> ambilDataNasabah();
}